package com.example.myapplication;


import androidx.annotation.NonNull;

import com.otaliastudios.cameraview.filter.BaseFilter;

public class BlurFilter2 extends BaseFilter {

    private static final String UNIFORM_TEXELWIDTH = "u_TexelWidth";
    private static final String UNIFORM_TEXELHEIGHT = "u_TexelHeight";
    private static final String UNIFORM_BLUR_SIZE = "u_BlurSize";


    private static final String FRAGMENT_SHADER =
//            "#extension GL_OES_EGL_image_external : require\n"
//            +"precision mediump float;\n"
//            +"uniform samplerExternalOES sTexture;\n"
//            + "varying vec2 "+DEFAULT_FRAGMENT_TEXTURE_COORDINATE_NAME+";\n"
//            +"uniform float "+UNIFORM_TEXELWIDTH+";\n"
//            +"uniform float "+UNIFORM_TEXELHEIGHT+";\n"
//            +"void main(){\n"
//            +"   vec2 singleStepOffset = vec2("+UNIFORM_TEXELWIDTH+", "+UNIFORM_TEXELHEIGHT+");\n"
//            +"   int multiplier = 0;\n"
//            +"   vec2 blurStep = vec2(0,0);\n"
//            +"   vec2 blurCoordinates[9];"
//            +"   for(int i = 0; i < 9; i++) {\n"
//            +"     multiplier = (i - 4);\n"
//            +"     blurStep = float(multiplier) * singleStepOffset;\n"
//            +"     blurCoordinates[i] = "+DEFAULT_FRAGMENT_TEXTURE_COORDINATE_NAME+".xy + blurStep;\n"
//            +"   }\n"
//            +"   vec3 sum = vec3(0,0,0);\n"
//            +"   vec4 color = texture2D(sTexture, blurCoordinates[4]);\n"
//            +"   sum += texture2D(sTexture, blurCoordinates[0]).rgb * 0.05;\n"
//            +"   sum += texture2D(sTexture, blurCoordinates[1]).rgb * 0.09;\n"
//            +"   sum += texture2D(sTexture, blurCoordinates[2]).rgb * 0.12;\n"
//            +"   sum += texture2D(sTexture, blurCoordinates[3]).rgb * 0.15;\n"
//            +"   sum += color.rgb * 0.18;\n"
//            +"   sum += texture2D(sTexture, blurCoordinates[5]).rgb * 0.15;\n"
//            +"   sum += texture2D(sTexture, blurCoordinates[6]).rgb * 0.12;\n"
//            +"   sum += texture2D(sTexture, blurCoordinates[7]).rgb * 0.09;\n"
//            +"   sum += texture2D(sTexture, blurCoordinates[8]).rgb * 0.05;\n"
//            +"   gl_FragColor = vec4(sum, color.a);\n"
//            +"}\n";
            "#extension GL_OES_EGL_image_external : require\n"
                    +"precision mediump float;\n"
                    +"uniform samplerExternalOES sceneTex;\n"
                    +"uniform float rt_w;\n"
                    +"uniform float rt_h;\n"
                    +"uniform float vx_offset;\n"
                    +"float offset[3] = float[3](0.0, 1.3846153846, 3.2307692308);\n"
                    +"float weight[3] = float[3](0.2270270270, 0.3162162162, 0.0702702703);\n"
                    +"varying vec2 "+DEFAULT_FRAGMENT_TEXTURE_COORDINATE_NAME+";\n"
                    +"void main() {\n"
                        +"vec3 sum = vec3(1.0, 0.0, 0.0);\n"
                        +"if ("+DEFAULT_FRAGMENT_TEXTURE_COORDINATE_NAME+".x < (vx_offset - 0.01)) {\n"
                            +"vec2 uv = "+DEFAULT_FRAGMENT_TEXTURE_COORDINATE_NAME+".xy;\n"
                            +"sum = texture2D(sceneTex, uv).rgb * weight[0];\n"
                            +"sum += texture2D(sceneTex, uv + vec2(0.0, offset[1])).rgb * weight[1];\n"
                            +"sum += texture2D(sceneTex, uv - vec2(0.0, offset[1])).rgb * weight[1];\n"
                            +"sum += texture2D(sceneTex, uv + vec2(0.0, offset[2])).rgb * weight[2];\n"
                            +"sum += texture2D(sceneTex, uv - vec2(0.0, offset[2])).rgb * weight[2];\n"
                        +"}\n"
                        +"else if ("+DEFAULT_FRAGMENT_TEXTURE_COORDINATE_NAME+".x >= (vx_offset + 0.01)) {\n"
                            +"sum = texture2D(sceneTex,"+DEFAULT_FRAGMENT_TEXTURE_COORDINATE_NAME+".xy).rgb;\n"
                        +"}\n"
                        +"gl_FragColor = vec4(sum, 1.0);\n"
                    +"}\n";

//
//    private final static String OFF_SHADER = "#extension GL_OES_EGL_image_external : require\n"
//           +"precision mediump float;\n"
//            +"uniform samplerExternalOES sTexture;\n"
//            +"varying vec2 "+DEFAULT_FRAGMENT_TEXTURE_COORDINATE_NAME+";\n"
//            +"uniform float "+UNIFORM_TEXELWIDTH+";\n"
//            +"uniform float "+UNIFORM_TEXELHEIGHT+";\n"
//            +"void main(){\n"
//            +"   vec2 firstOffset = vec2(1.3846153846 * "+UNIFORM_TEXELWIDTH+", 1.3846153846 * "+UNIFORM_TEXELHEIGHT+");\n"
//            +"   vec2 secondOffset = vec2(3.2307692308 * "+UNIFORM_TEXELWIDTH+", 3.2307692308 * "+UNIFORM_TEXELHEIGHT+");\n"
//            +"   vec3 sum = vec3(0,0,0);\n"
//            +"   vec4 color = texture2D(sTexture, "+DEFAULT_FRAGMENT_TEXTURE_COORDINATE_NAME+");\n"
//            +"   sum += color.rgb * 0.2270270270;\n"
//            +"   sum += texture2D(sTexture, "+DEFAULT_FRAGMENT_TEXTURE_COORDINATE_NAME+" - firstOffset).rgb * 0.3162162162;\n"
//            +"   sum += texture2D(sTexture, "+DEFAULT_FRAGMENT_TEXTURE_COORDINATE_NAME+" + firstOffset).rgb * 0.3162162162;\n"
//            +"   sum += texture2D(sTexture, "+DEFAULT_FRAGMENT_TEXTURE_COORDINATE_NAME+" - secondOffset).rgb * 0.0702702703;\n"
//            +"   sum += texture2D(sTexture, "+DEFAULT_FRAGMENT_TEXTURE_COORDINATE_NAME+" + secondOffset).rgb * 0.0702702703;\n"
//            +"   gl_FragColor = vec4(sum, color.a);\n"
//            +"}\n";


    public BlurFilter2() {
    }

    @NonNull
    @Override
    public String getFragmentShader() {
        return FRAGMENT_SHADER;
    }


}
