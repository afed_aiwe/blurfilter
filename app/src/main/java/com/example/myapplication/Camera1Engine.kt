package com.example.myapplication

import android.hardware.Camera
import android.util.Log
import com.google.android.gms.tasks.Task
import com.otaliastudios.cameraview.CameraOptions
import com.otaliastudios.cameraview.engine.Camera1Engine
import java.lang.reflect.Field

class Camera1Engine : Camera1Engine {

    constructor(callback: Callback) : super(callback)

    private var camera: Camera? = null

    private var field: Field? = null

    private var updatePreview: ((data: ByteArray, width: Int, height: Int) -> Unit)? = null

    private fun getCamera() {
        try {
            val example = Class.forName("com.otaliastudios.cameraview.engine.Camera1Engine")
            val field = example.getDeclaredField("mCamera")
            field.isAccessible = true
            camera = field.get(this) as? Camera
            this.field = field
        } catch (ex: NoSuchFieldException) {
            Log.e("TAG", "exc", ex)
        } catch (ex: IllegalAccessException) {
            Log.e("TAG", "exc", ex)
        }
    }

    override fun onStartPreview(): Task<Void> {
        val startPreview = super.onStartPreview()
        val params = camera?.parameters
        if (params?.isVideoStabilizationSupported == true) {
            params.videoStabilization = true
            Log.e("TAG", "Camera1Engine device is support video stabilization")
        } else {
            Log.e("TAG", "Camera1Engine device not support video stabilization")
        }
        camera?.parameters = params
        hasInstalledVideoStabilization()
        return startPreview
    }

    override fun onStartEngine(): Task<Void> {
        val startEngine = super.onStartEngine()
        getCamera()
        val params = camera?.parameters
        if (params?.isVideoStabilizationSupported == true) {
            params.videoStabilization = true
            Log.e("TAG","Camera1Engine device is support video stabilization")
        } else {
            Log.e("TAG","Camera1Engine device not support video stabilization")
        }
        camera?.parameters = params
        hasInstalledVideoStabilization()
        return startEngine
    }

    override fun onPreviewFrame(data: ByteArray?, camera: Camera?) {
        super.onPreviewFrame(data, camera)
        if (data != null && camera != null) {
            val size = camera.parameters.previewSize
            updatePreview?.invoke(data, size.width, size.height)
        }
    }

    fun setUpdateFrame(updateFrame: (data: ByteArray, width: Int, height: Int) -> Unit) {
        this.updatePreview = updateFrame
    }

    fun hasInstalledVideoStabilization(): Boolean {
        return if (camera?.parameters?.videoStabilization == true) {
            Log.e("TAG","Camera1Engine: video stabilization enable")
            true
        } else {
            Log.e("TAG","Camera1Engine: video stabilization disable")
            false
        }
    }
}