package com.example.myapplication

import android.content.Context
import android.util.AttributeSet
import com.otaliastudios.cameraview.CameraView
import com.otaliastudios.cameraview.controls.Engine
import com.otaliastudios.cameraview.engine.CameraEngine

class ExteriorCameraView : CameraView {

    private lateinit var cameraEngine: Camera1Engine

    var onPreviewFrame: ((data: ByteArray, width: Int, height: Int) -> Unit)? = null

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    override fun instantiateCameraEngine(engine: Engine, callback: CameraEngine.Callback): CameraEngine {
        cameraEngine = Camera1Engine(callback)
        return cameraEngine
    }

    fun hasInstalledVideoStabilization(): Boolean {
        return cameraEngine.hasInstalledVideoStabilization()
    }

    fun setOnPreviewFrameListener(onPreviewFrame: (data: ByteArray, width: Int, height: Int) -> Unit) {
        cameraEngine.setUpdateFrame(onPreviewFrame)
    }
}