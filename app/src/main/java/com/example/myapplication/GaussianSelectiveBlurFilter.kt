package com.example.myapplication

import android.graphics.PointF
import android.opengl.GLES20
import android.util.Log
import com.otaliastudios.cameraview.filter.BaseFilter
import com.otaliastudios.cameraview.internal.GlUtils
import project.android.imageprocessing.GLRenderer

class GaussianSelectiveBlurFilter(blurSize: Float, aspectRatio: Float, excludedCirclePoint: PointF, excludedCircleRadius: Float, excludedBlurSize: Float) : BaseFilter() {
    companion object {
        private const val TAG = "GaussianBlurFilter"

        const val UNIFORM_TEXTUREBASE = "u_Texture"
        const val UNIFORM_BLUR_SIZE = "u_BlurSize"
        const val UNIFORM_ASPECT_RATIO = "u_AspectRatio"
        const val UNIFORM_EXCLUDE_CIRCLE_POINT = "u_ExcludeCirclePoint"
        const val UNIFORM_EXCLUDE_CIRCLE_RADIUS = "u_ExcludeCircleRadius"
        const val UNIFORM_TEXTURE0 = UNIFORM_TEXTUREBASE + 0

        const val ATTRIBUTE_POSITION = "a_Position"
        const val ATTRIBUTE_TEXCOORD = "a_TexCoord"
        const val VARYING_TEXCOORD = "v_TexCoord"
        const val DEFAULT_VERTEX_MVP_MATRIX_NAME = "uMVPMatrix"
        const val DEFAULT_VERTEX_TRANSFORM_MATRIX_NAME = "uTexMatrix"
    }

    private var blurSize = -1f
    private var aspectRatio = -1f
    private var excludedCirclePoint: PointF
    private var excludedCircleRadius = -1f
    private var blurSizeHandle = -1
    private var aspectRatioHandle = -1
    private var excludedCirclePointHandle = -1
    private var excludedCircleRadiusHandle = -1

    private var programHandle: Int = -1
    private var vertexPositionLocation = -1
    private var vertexTextureCoordinateLocation = -1
    private var vertexModelViewProjectionMatrixLocation = -1
    private var vertexTransformMatrixLocation = -1

    init {
        this.blurSize = excludedBlurSize
        this.aspectRatio = aspectRatio
        this.excludedCirclePoint = excludedCirclePoint
        this.excludedCircleRadius = excludedCircleRadius
    }

    override fun getFragmentShader(): String = "#extension GL_OES_EGL_image_external : require\n" +
            "precision mediump float;\n" +
            "uniform sampler2D $UNIFORM_TEXTURE0;\n" +
            "uniform sampler2D ${UNIFORM_TEXTUREBASE + 1};\n" +
            "varying vec2 $VARYING_TEXCOORD;\n" +
            "uniform float $UNIFORM_BLUR_SIZE;\n" +
            "uniform float $UNIFORM_ASPECT_RATIO;\n" +
            "uniform vec2 $UNIFORM_EXCLUDE_CIRCLE_POINT;\n" +
            "uniform float $UNIFORM_EXCLUDE_CIRCLE_RADIUS;\n" +

            "void main(){\n"+
            "   vec4 sharpImageColor = texture2D($UNIFORM_TEXTURE0, $VARYING_TEXCOORD);\n"+
            "   vec4 blurredImageColor = texture2D(${UNIFORM_TEXTUREBASE + 1}, $VARYING_TEXCOORD);\n"+
            "   vec2 texCoordAfterAspect = vec2($VARYING_TEXCOORD.x, $VARYING_TEXCOORD.y * $UNIFORM_ASPECT_RATIO + 0.5 - 0.5 * $UNIFORM_ASPECT_RATIO);\n"+
            "   float distanceFromCenter = distance($UNIFORM_EXCLUDE_CIRCLE_POINT, texCoordAfterAspect);\n"+
            "   gl_FragColor = mix(sharpImageColor, blurredImageColor, smoothstep($UNIFORM_EXCLUDE_CIRCLE_RADIUS - $UNIFORM_BLUR_SIZE, $UNIFORM_EXCLUDE_CIRCLE_RADIUS, distanceFromCenter));\n"+
            "}\n"

    override fun createDefaultVertexShader(): String =
        "attribute vec4 ${ATTRIBUTE_POSITION};\n"+
                "attribute vec2 $ATTRIBUTE_TEXCOORD;\n"+
                "varying vec2 $VARYING_TEXCOORD;\n"+

                "void main(){\n" +
                "$VARYING_TEXCOORD = ${ATTRIBUTE_TEXCOORD};\n" +
                "gl_Position = ${ATTRIBUTE_POSITION};\n" +
                "}\n"

    override fun createDefaultFragmentShader(): String =
        "#extension GL_OES_EGL_image_external : require\n" +
                "precision mediump float;\n" +
                "uniform sampler2D ${UNIFORM_TEXTURE0};\n" +
                "varying vec2 ${VARYING_TEXCOORD};\n" +
                "void main(){\n" +
                "gl_FragColor = texture2D(${GLRenderer.UNIFORM_TEXTURE0},${GLRenderer.VARYING_TEXCOORD});\n" +
                "}\n"

    override fun onCreate(programHandle: Int) {
        getPrivateField()
        this.programHandle = programHandle
        vertexPositionLocation = GLES20.glGetAttribLocation(programHandle, ATTRIBUTE_POSITION)
        GlUtils.checkLocation(vertexPositionLocation, ATTRIBUTE_POSITION)
        vertexTextureCoordinateLocation = GLES20.glGetAttribLocation(programHandle, ATTRIBUTE_TEXCOORD)
        GlUtils.checkLocation(vertexTextureCoordinateLocation, ATTRIBUTE_TEXCOORD)
        vertexModelViewProjectionMatrixLocation = GLES20.glGetUniformLocation(programHandle, DEFAULT_VERTEX_MVP_MATRIX_NAME)
        GlUtils.checkLocation(vertexModelViewProjectionMatrixLocation, DEFAULT_VERTEX_MVP_MATRIX_NAME)
        vertexTransformMatrixLocation = GLES20.glGetUniformLocation(programHandle, DEFAULT_VERTEX_TRANSFORM_MATRIX_NAME)
        GlUtils.checkLocation(vertexTransformMatrixLocation, DEFAULT_VERTEX_TRANSFORM_MATRIX_NAME)

        blurSizeHandle = GLES20.glGetUniformLocation(
            programHandle,
            UNIFORM_BLUR_SIZE
        )
        GlUtils.checkLocation(blurSizeHandle, UNIFORM_BLUR_SIZE)
        aspectRatioHandle = GLES20.glGetUniformLocation(
            programHandle,
            UNIFORM_ASPECT_RATIO
        )
        GlUtils.checkLocation(aspectRatioHandle, UNIFORM_ASPECT_RATIO)
        excludedCirclePointHandle = GLES20.glGetUniformLocation(
            programHandle,
            UNIFORM_EXCLUDE_CIRCLE_POINT
        )
        GlUtils.checkLocation(excludedCirclePointHandle, UNIFORM_EXCLUDE_CIRCLE_POINT)
        excludedCircleRadiusHandle = GLES20.glGetUniformLocation(
            programHandle,
            UNIFORM_EXCLUDE_CIRCLE_RADIUS
        )
        GlUtils.checkLocation(excludedCircleRadiusHandle, UNIFORM_EXCLUDE_CIRCLE_RADIUS)
    }

    override fun onDestroy() {
        blurSize = 0f
        blurSize = 0f
        aspectRatio = 0f
        excludedCircleRadius = 0f
        blurSizeHandle = 0
        aspectRatioHandle = 0
        excludedCirclePointHandle = 0
        excludedCircleRadiusHandle = 0
        super.onDestroy()
    }

    override fun onPreDraw(timestampUs: Long, transformMatrix: FloatArray?) {
        super.onPreDraw(timestampUs, transformMatrix)
        GLES20.glUniform1f(blurSizeHandle, blurSize)
        GlUtils.checkError("glUniform1f")
        GLES20.glUniform1f(aspectRatioHandle, aspectRatio)
        GlUtils.checkError("glUniform1f")
        GLES20.glUniform1f(excludedCircleRadiusHandle, excludedCircleRadius)
        GlUtils.checkError("glUniform1f")
        GLES20.glUniform2f(
            excludedCirclePointHandle,
            excludedCirclePoint.x,
            excludedCirclePoint.y
        )
        GlUtils.checkError("glUniform1f")
    }

    private fun getPrivateField() {
        try {
            val example = Class.forName("com.otaliastudios.cameraview.filter.BaseFilter")
            var field = example.getDeclaredField("programHandle")
            field.isAccessible = true
            programHandle = field.get(this) as? Int ?: -1
            field = example.getDeclaredField("vertexPositionLocation")
            field.isAccessible = true
            vertexPositionLocation = field.get(this) as? Int ?: - 1
            field = example.getDeclaredField("vertexTextureCoordinateLocation")
            field.isAccessible = true
            vertexTextureCoordinateLocation = field.get(this) as? Int ?: -1
            field = example.getDeclaredField("vertexModelViewProjectionMatrixLocation")
            field.isAccessible = true
            vertexModelViewProjectionMatrixLocation = field.get(this) as? Int ?: -1
            field = example.getDeclaredField("vertexTransformMatrixLocation")
            field.isAccessible = true
            vertexTransformMatrixLocation = field.get(this) as? Int ?: -1

        } catch (ex: NoSuchFieldException) {
            Log.e(TAG, "NoSuchFieldException", ex)
        } catch (ex: IllegalAccessException) {
            Log.e(TAG, "IllegalAccessException", ex)
        }
    }
}