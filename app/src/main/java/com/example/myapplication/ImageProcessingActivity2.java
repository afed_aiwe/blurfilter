package com.example.myapplication;

import android.app.Activity;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.Environment;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import project.android.imageprocessing.FastImageProcessingPipeline;
import project.android.imageprocessing.FastImageProcessingView;
import project.android.imageprocessing.filter.BasicFilter;
import project.android.imageprocessing.filter.processing.BoxBlurFilter;
import project.android.imageprocessing.filter.processing.FastBlurFilter;
import project.android.imageprocessing.filter.processing.GaussianBlurFilter;
import project.android.imageprocessing.filter.processing.GaussianBlurPositionFilter;
import project.android.imageprocessing.filter.processing.GaussianSelectiveBlurFilter;
import project.android.imageprocessing.filter.processing.MotionBlurFilter;
import project.android.imageprocessing.input.VideoResourceInput;
import project.android.imageprocessing.output.JPGFileEndpoint;
import project.android.imageprocessing.output.ScreenEndpoint;

public class ImageProcessingActivity2 extends Activity {
    private FastImageProcessingView view;
    private BasicFilter edgeDetect;
    private FastImageProcessingPipeline pipeline;
    private VideoResourceInput video;
    private JPGFileEndpoint image;
    private ScreenEndpoint screen;
    private long touchTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        view = new FastImageProcessingView(this);
        pipeline = new FastImageProcessingPipeline();
        video = new VideoResourceInput(view, this, R.raw.birds);
        edgeDetect = new GaussianSelectiveBlurFilter(4f, 1.2f, new PointF(0.4f,0.5f), 0.5f, 0.1f);
        image = new JPGFileEndpoint(this, false, Environment.getExternalStorageDirectory().getAbsolutePath()+"/Pictures/outputImage", false);
        screen = new ScreenEndpoint(pipeline);
        video.addTarget(edgeDetect);
        edgeDetect.addTarget(image);
        edgeDetect.addTarget(screen);
        pipeline.addRootRenderer(video);
        view.setPipeline(pipeline);
        setContentView(view);
        pipeline.startRendering();
        video.startWhenReady();
        view.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent me) {
                if(System.currentTimeMillis() - 100 > touchTime) {
                    touchTime = System.currentTimeMillis();
                    if(video.isPlaying()) {
                        video.stop();
                    } else {
                        video.startWhenReady();
                    }
                }
                return true;
            }

        });
    }

}
