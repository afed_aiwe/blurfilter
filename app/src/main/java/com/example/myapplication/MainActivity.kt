package com.example.myapplication

import android.os.Build
import android.os.Bundle
import android.util.SizeF
import android.view.View
import android.view.ViewTreeObserver
import androidx.appcompat.app.AppCompatActivity
import com.gpu_blur.GPUImageView
import com.gpu_blur.filters.GPUImageGaussianBlurFilter
import com.gpu_blur.filters.GPUImageGaussianRoundRectFilter
import com.otaliastudios.cameraview.CameraException
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.CameraView
import com.otaliastudios.cameraview.frame.FrameProcessor
import kotlinx.android.synthetic.main.activity_main.*

//import jp.co.cyberagent.android.gpuimage.GPUImageView
//import jp.co.cyberagent.android.gpuimage.filter.GPUImageGaussianBlurFilter

class MainActivity : AppCompatActivity() {

    private val finder: ExteriorCameraView by lazy { findViewById<ExteriorCameraView>(R.id.viewFinder) }
    private val gpuImageView: GPUImageView by lazy { findViewById<GPUImageView>(R.id.blurView) }

    private val cameraListener = object : CameraListener() {
        override fun onVideoRecordingStart() {
            super.onVideoRecordingStart()
        }

        override fun onVideoRecordingEnd() {
            super.onVideoRecordingEnd()
        }

        override fun onCameraError(exception: CameraException) {
        }
    }
//        private val frameProcessor = FrameProcessor { frame ->
//        gpuImageView.updatePreviewFrame(frame.freeze().data, frame.freeze().size.width, frame.freeze().size.height)
//    }

//    private val previewFrame = fun(data: ByteArray, width: Int, height: Int) {
//        gpuImageView.updatePreviewFrame(data, width, height)
//    }

    // https://thebookofshaders.com/glossary/?search=smoothstep
    //https://pastebin.com/DLF1WW0U
    //https://www.shadertoy.com/view/3tj3Dm#
    //https://www.shadertoy.com/view/WtX3DN
    //https://gamedev.ru/code/forum/?id=243874
    //https://github.com/cats-oss/android-gpuimage
    //https://github.com/MasayukiSuda/GPUVideo-android
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        finder.setLifecycleOwner(this)
        finder.addCameraListener(cameraListener)
        setupBlurFilter()
    }

    private fun setupBlurFilter() {
        finder.onGlobalLayoutChanged {
            val width = finder.width
            val height = finder.height
            val left = width / 8f
            val top = height / 7f
            val bottom = height - top
            val right = width - left
            val rectWidth = right - left
            val rectHeight = bottom - top
            val cornerRadius = top / 2
            val groupFilters =
                GPUImageGaussianRoundRectFilter(
                    25f,
                    SizeF(rectWidth, rectHeight),
                    cornerRadius,
                    SizeF(width.toFloat(), height.toFloat())
                )
//            val groupFilters = GPUImageGaussianSelectiveBlurFilter(50f, SizeF(rectWidth, rectHeight), cornerRadius, SizeF(width.toFloat(), height.toFloat()))
//            val groupFilters = GPUImageGaussianSelectiveBlurFilter()
//            val groupFilters = GPUImageGaussianBlurFilter(25f)
//            val groupFilters = GPUImageGaussianBlurFilter(15f)
            gpuImageView.filter = groupFilters
//            finder.addFrameProcessor(frameProcessor)
            gpuImageView.setRenderMode(GPUImageView.RENDERMODE_CONTINUOUSLY)
            finder.setOnPreviewFrameListener { data, width, height ->
                gpuImageView.updatePreviewFrame(data, width, height)
            }
        }
    }

    override fun onDestroy() {
        finder.removeCameraListener(cameraListener)
        finder.destroy()
//        finder.removeFrameProcessor(frameProcessor)
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        finder.open()
//        gpuImageView.onResume()
    }

    override fun onPause() {
//        gpuImageView.onPause()
        finder.close()
        super.onPause()
    }

    fun View?.onGlobalLayoutChanged(action: () -> Unit) {
        if (this == null) {
            return
        }
        viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    viewTreeObserver.removeOnGlobalLayoutListener(this)
                } else {
                    @Suppress("DEPRECATION")
                    viewTreeObserver.removeGlobalOnLayoutListener(this)
                }
                action()
            }
        })
    }
}
