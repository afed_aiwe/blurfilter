package com.example.myapplication

import android.opengl.GLES20
import com.otaliastudios.cameraview.filter.BaseFilter
import com.otaliastudios.cameraview.internal.GlUtils

class SimpleBlurFilter(private val blurSize: Float) : BaseFilter() {
    private var blurSizeLocation = -1
    private var texelWidthLocation = -1
    private var texelHeightLocation = -1

    private var texelWidth = 0f
    private var texelHeight = 0f
    private var width = 1920
    private var height = 1080
    override fun getFragmentShader(): String = FRAGMENT_SHADER

    override fun onCreate(programHandle: Int) {
        super.onCreate(programHandle)
        texelWidthLocation = GLES20.glGetUniformLocation(programHandle, UNIFORM_TEXELWIDTH)
        GlUtils.checkLocation(texelWidthLocation, UNIFORM_TEXELWIDTH)
        texelHeightLocation = GLES20.glGetUniformLocation(programHandle, UNIFORM_TEXELHEIGHT)
        GlUtils.checkLocation(texelHeightLocation, UNIFORM_TEXELHEIGHT)
        blurSizeLocation = GLES20.glGetUniformLocation(programHandle, UNIFORM_BLUR_SIZE)
        GlUtils.checkLocation(blurSizeLocation, UNIFORM_BLUR_SIZE)
    }

    override fun setSize(width: Int, height: Int) {
        super.setSize(width, height)
        this.width = width
        this.height = height
        texelWidth = 1.0f / width
        texelHeight = 1.0f / height
    }

    override fun onPreDraw(timestampUs: Long, transformMatrix: FloatArray?) {
        val currentPass = 1
        if(currentPass == 1) {
            texelWidth = 1.0f / width.toFloat()
            texelHeight = 0f;
        } else {
            texelWidth = 0f;
            texelHeight = 1.0f / height.toFloat()
        }
        super.onPreDraw(timestampUs, transformMatrix)
        GLES20.glUniform1f(texelWidthLocation, texelWidth)
        GlUtils.checkError("glUniform1f")
        GLES20.glUniform1f(texelHeightLocation, texelHeight)
        GlUtils.checkError("glUniform1f")
        GLES20.glUniform1f(blurSizeLocation, blurSize)
        GlUtils.checkError("glUniform1f")
    }

    override fun onDestroy() {
        blurSizeLocation = -1
        texelHeightLocation = -1
        texelWidthLocation = -1
        super.onDestroy()
    }

    companion object {
        private const val UNIFORM_BLUR_SIZE = "uBlurSize"
        private const val UNIFORM_TEXELWIDTH = "uTexelWidth"
        private const val UNIFORM_TEXELHEIGHT = "uTexelHeight"

        private const val FRAGMENT_SHADER =
            "#extension GL_OES_EGL_image_external : require\n" +
                    "precision mediump float;\n"+
                    "uniform samplerExternalOES tex_sampler_0;\n"+
                    "varying vec2 $DEFAULT_FRAGMENT_TEXTURE_COORDINATE_NAME;\n"+
                    "uniform float $UNIFORM_BLUR_SIZE;\n"+
                    "uniform float $UNIFORM_TEXELWIDTH;\n"+
                    "uniform float $UNIFORM_TEXELHEIGHT;\n"+

                    "void main() {\n"+
                    "   vec2 singleStepOffset = vec2($UNIFORM_TEXELWIDTH, $UNIFORM_TEXELHEIGHT);\n"+
                    "   int multiplier = 0;\n"+
                    "   vec2 blurStep = vec2(0,0);\n"+
                    "   vec2 blurCoordinates[9];   for(int i = 0; i < 9; i++) {\n"+
                    "       multiplier = (i - 4);\n"+
                    "       blurStep = float(multiplier) * singleStepOffset;\n"+
                    "       blurCoordinates[i] = $DEFAULT_FRAGMENT_TEXTURE_COORDINATE_NAME.xy + blurStep;\n"+
                    "   }\n"+
                    "   vec3 sum = vec3(0,0,0);\n"+
                    "   vec4 color = texture2D(tex_sampler_0, blurCoordinates[4]);\n"+
                    "   sum += texture2D(tex_sampler_0, blurCoordinates[0]).rgb * 0.05;\n"+
                    "   sum += texture2D(tex_sampler_0, blurCoordinates[1]).rgb * 0.09;\n"+
                    "   sum += texture2D(tex_sampler_0, blurCoordinates[2]).rgb * 0.12;\n"+
                    "   sum += texture2D(tex_sampler_0, blurCoordinates[3]).rgb * 0.15;\n"+
                    "   sum += color.rgb * 0.18;\n"+
                    "   sum += texture2D(tex_sampler_0, blurCoordinates[5]).rgb * 0.15;\n"+
                    "   sum += texture2D(tex_sampler_0, blurCoordinates[6]).rgb * 0.12;\n"+
                    "   sum += texture2D(tex_sampler_0, blurCoordinates[7]).rgb * 0.09;\n"+
                    "   sum += texture2D(tex_sampler_0, blurCoordinates[8]).rgb * 0.05;\n"+
                    "   gl_FragColor = vec4(sum, color.a);\n"+
                    "}\n"
    }
}