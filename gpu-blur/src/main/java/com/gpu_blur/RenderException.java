package com.gpu_blur;

public class RenderException extends RuntimeException {
    public RenderException(String msg) {
        super(msg);
    }
}