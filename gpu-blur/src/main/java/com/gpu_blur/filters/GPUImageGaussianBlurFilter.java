package com.gpu_blur.filters;


public class GPUImageGaussianBlurFilter extends GPUImageTwoPassTextureSamplingFilter {
    public static final String FRAGMENT_SHADER = "uniform sampler2D inputImageTexture;\n" +
            "const int GAUSSIAN_SAMPLES = 9;\n" +
            "varying vec2 textureCoordinate;\n" +
            "varying vec2 blurCoordinates[GAUSSIAN_SAMPLES];\n" +
            "void main()\n" +
            "{\n" +
            "vec3 sum = vec3(0.0);\n" +
            "    vec4 fragColor=texture2D(inputImageTexture,textureCoordinate);\n" +
            "    sum += texture2D(inputImageTexture, blurCoordinates[0]).rgb * 0.05;\n" +
            "    sum += texture2D(inputImageTexture, blurCoordinates[1]).rgb * 0.09;\n" +
            "    sum += texture2D(inputImageTexture, blurCoordinates[2]).rgb * 0.12;\n" +
            "    sum += texture2D(inputImageTexture, blurCoordinates[3]).rgb * 0.15;\n" +
            "    sum += texture2D(inputImageTexture, blurCoordinates[4]).rgb * 0.18;\n" +
            "    sum += texture2D(inputImageTexture, blurCoordinates[5]).rgb * 0.15;\n" +
            "    sum += texture2D(inputImageTexture, blurCoordinates[6]).rgb * 0.12;\n" +
            "    sum += texture2D(inputImageTexture, blurCoordinates[7]).rgb * 0.09;\n" +
            "    sum += texture2D(inputImageTexture, blurCoordinates[8]).rgb * 0.05;\n" +
            "gl_FragColor = vec4(sum,fragColor.a);\n" +
            "}";
    public static final String VERTEX_SHADER =
            "attribute vec4 position;\n" +
            "attribute vec4 inputTextureCoordinate;\n" +
            "const int GAUSSIAN_SAMPLES = 9;\n" +
            "uniform float texelWidthOffset;\n" +
            "uniform float texelHeightOffset;\n" +
            "varying vec2 textureCoordinate;\n" +
            "varying vec2 blurCoordinates[GAUSSIAN_SAMPLES];\n" +
            "void main()\n{\n\tgl_Position = position;\n" +
            "textureCoordinate = inputTextureCoordinate.xy;\n" +
            "int multiplier = 0;\n" +
            "vec2 blurStep;\n" +
            "   vec2 singleStepOffset = vec2(texelHeightOffset, texelWidthOffset);\n" +
            "    for (int i = 0; i < GAUSSIAN_SAMPLES; i++)\n" +
            "   {\n" +
            "multiplier = (i - ((GAUSSIAN_SAMPLES - 1) / 2));\n" +
            "      blurStep = float(multiplier) * singleStepOffset;\n" +
            "blurCoordinates[i] = inputTextureCoordinate.xy + blurStep;\n" +
            "}\n" +
            "}\n";

    protected float mBlurSize;

    static String vertexShaderForBlur(float fBlurRadius) {
        if (fBlurRadius < 1) throw new IllegalArgumentException("Blur radius must be >= 1");
        int blurRadiusInPixels = Math.round(fBlurRadius);

        int calculatedSampleRadius = calculateSampleRadius(blurRadiusInPixels);
        if (blurRadiusInPixels >= 1) // Avoid a divide-by-zero error here
        {
            // Calculate the number of pixels to sample from by setting a bottom limit for the contribution of the outermost pixel
            float minimumWeightToFindEdgeOfSamplingArea = 1.0f/256.0f;
            calculatedSampleRadius = (int)Math.floor(Math.sqrt(-2.0 * Math.pow(blurRadiusInPixels, 2.0) * Math.log(minimumWeightToFindEdgeOfSamplingArea * Math.sqrt(2.0 * Math.PI * Math.pow(blurRadiusInPixels, 2.0))) ));
            calculatedSampleRadius += calculatedSampleRadius % 2; // There's nothing to gain from handling odd radius sizes, due to the optimizations I use
        }
        return vertexShaderForBlur(calculatedSampleRadius, (float) blurRadiusInPixels);
    }

    static String fragmentShaderForBlur(float fBlurRadius) {
        int blurRadiusInPixels = Math.round(fBlurRadius);
        int calculatedSampleRadius = calculateSampleRadius(blurRadiusInPixels);
        return fragmentShaderForBlur(calculatedSampleRadius, (float) blurRadiusInPixels);
    }

    static int calculateSampleRadius(int blurRadiusInPixels) {
        int calculatedSampleRadius = 0;
        // Calculate the number of pixels to sample from by setting a bottom limit for the contribution of the outermost pixel
        float minimumWeightToFindEdgeOfSamplingArea = 1.0f/256.0f;
        calculatedSampleRadius = (int)Math.floor(Math.sqrt(-2.0 * Math.pow(blurRadiusInPixels, 2.0) * Math.log(minimumWeightToFindEdgeOfSamplingArea * Math.sqrt(2.0 * Math.PI * Math.pow(blurRadiusInPixels, 2.0))) ));
        calculatedSampleRadius += calculatedSampleRadius % 2; // There's nothing to gain from handling odd radius sizes, due to the optimizations I use
        return calculatedSampleRadius;
    }

    static String vertexShaderForBlur(int blurRadius, float sigma) {
        // First, generate the normal Gaussian weights for a given sigma
        float [] standardGaussianWeights = new float[blurRadius + 1];
        float sumOfWeights = 0.0f;
        for (int currentGaussianWeightIndex = 0; currentGaussianWeightIndex < blurRadius + 1; currentGaussianWeightIndex++)
        {
            standardGaussianWeights[currentGaussianWeightIndex] = (float) ((1.0 / Math.sqrt(2.0 * Math.PI * Math.pow(sigma, 2.0))) * Math.exp(-Math.pow(currentGaussianWeightIndex, 2.0) / (2.0 * Math.pow(sigma, 2.0))));

            if (currentGaussianWeightIndex == 0)
            {
                sumOfWeights += standardGaussianWeights[currentGaussianWeightIndex];
            }
            else
            {
                sumOfWeights += 2.0 * standardGaussianWeights[currentGaussianWeightIndex];
            }
        }

        // Next, normalize these weights to prevent the clipping of the Gaussian curve at the end of the discrete samples from reducing luminance
        for (int currentGaussianWeightIndex = 0; currentGaussianWeightIndex < blurRadius + 1; currentGaussianWeightIndex++)
        {
            standardGaussianWeights[currentGaussianWeightIndex] = standardGaussianWeights[currentGaussianWeightIndex] / sumOfWeights;
        }

        // From these weights we calculate the offsets to read interpolated values from
        int numberOfOptimizedOffsets = Math.min(blurRadius / 2 + (blurRadius % 2), 7);
        float [] optimizedGaussianOffsets = new float[numberOfOptimizedOffsets];

        for (int currentOptimizedOffset = 0; currentOptimizedOffset < numberOfOptimizedOffsets; currentOptimizedOffset++)
        {
            float firstWeight = standardGaussianWeights[currentOptimizedOffset*2 + 1];
            float secondWeight = standardGaussianWeights[currentOptimizedOffset*2 + 2];

            float optimizedWeight = firstWeight + secondWeight;

            optimizedGaussianOffsets[currentOptimizedOffset] = (firstWeight * (currentOptimizedOffset*2 + 1) + secondWeight * (currentOptimizedOffset*2 + 2)) / optimizedWeight;
        }

        String shaderString = "attribute vec4 position;\n"
                + "attribute vec4 inputTextureCoordinate;\n"
                + "\n"
                + "uniform float texelWidthOffset;\n"
                + "uniform float texelHeightOffset;\n"
                + "\n"
                + "varying vec2 textureCoordinate;\n"
                + "varying vec2 blurCoordinates[" + (long)(1 + (numberOfOptimizedOffsets * 2)) +"];\n"
                + "\n"
                + "void main()\n"
                + "{\n"
                + "    gl_Position = position;\n"
                + "   textureCoordinate = inputTextureCoordinate.xy;\n"
                + "    \n"
                + "    vec2 singleStepOffset = vec2(texelWidthOffset, texelHeightOffset);\n";

        // Inner offset loop
        shaderString += "    blurCoordinates[0] = inputTextureCoordinate.xy;\n";
        for (int currentOptimizedOffset = 0; currentOptimizedOffset < numberOfOptimizedOffsets; currentOptimizedOffset++)
        {
            shaderString += "    blurCoordinates[" + (long)((currentOptimizedOffset * 2) + 1) + "] = inputTextureCoordinate.xy + singleStepOffset * " + optimizedGaussianOffsets[currentOptimizedOffset] + ";\n"
                    +  "    blurCoordinates[" + (long)((currentOptimizedOffset * 2) + 2) + "] = inputTextureCoordinate.xy - singleStepOffset * " + optimizedGaussianOffsets[currentOptimizedOffset] + ";\n";
        }

        // Footer
        shaderString += "}\n";
        return shaderString;
    }

    static String fragmentShaderForBlur(int blurRadius, float sigma) {
        // First, generate the normal Gaussian weights for a given sigma
        float [] standardGaussianWeights = new float[blurRadius + 1];
        float sumOfWeights = 0.0f;
        for (int currentGaussianWeightIndex = 0; currentGaussianWeightIndex < blurRadius + 1; currentGaussianWeightIndex++)
        {
            standardGaussianWeights[currentGaussianWeightIndex] = (float) ((1.0 / Math.sqrt(2.0 * Math.PI * Math.pow(sigma, 2.0))) * Math.exp(-Math.pow(currentGaussianWeightIndex, 2.0) / (2.0 * Math.pow(sigma, 2.0))));

            if (currentGaussianWeightIndex == 0)
            {
                sumOfWeights += standardGaussianWeights[currentGaussianWeightIndex];
            }
            else
            {
                sumOfWeights += 2.0 * standardGaussianWeights[currentGaussianWeightIndex];
            }
        }

        // Next, normalize these weights to prevent the clipping of the Gaussian curve at the end of the discrete samples from reducing luminance
        for (int currentGaussianWeightIndex = 0; currentGaussianWeightIndex < blurRadius + 1; currentGaussianWeightIndex++)
        {
            standardGaussianWeights[currentGaussianWeightIndex] = standardGaussianWeights[currentGaussianWeightIndex] / sumOfWeights;
        }

        // From these weights we calculate the offsets to read interpolated values from
        int numberOfOptimizedOffsets = Math.min(blurRadius / 2 + (blurRadius % 2), 7);
        int trueNumberOfOptimizedOffsets = blurRadius / 2 + (blurRadius % 2);

        // Header

        String shaderString = "uniform sampler2D inputImageTexture;\n"
                + "uniform float texelWidthOffset;\n"
                + "uniform float texelHeightOffset;\n"
                + "\n"
                + "varying vec2 blurCoordinates[" + (1 + (numberOfOptimizedOffsets * 2)) + "];\n"
                + "varying vec2 textureCoordinate;\n"
                + "\n"
                + "void main()\n"
                + "{\n"
                + "   vec3 sum = vec3(0.0);\n"
                + "   vec4 fragColor=texture2D(inputImageTexture,textureCoordinate);\n";

        // Inner texture loop
        shaderString += "    sum += texture2D(inputImageTexture, blurCoordinates[0]).rgb * " + standardGaussianWeights[0] + ";\n";

        for (int currentBlurCoordinateIndex = 0; currentBlurCoordinateIndex < numberOfOptimizedOffsets; currentBlurCoordinateIndex++)
        {
            float firstWeight = standardGaussianWeights[currentBlurCoordinateIndex * 2 + 1];
            float secondWeight = standardGaussianWeights[currentBlurCoordinateIndex * 2 + 2];
            float optimizedWeight = firstWeight + secondWeight;

            shaderString += "    sum += texture2D(inputImageTexture, blurCoordinates[" + ((currentBlurCoordinateIndex * 2) + 1) + "]).rgb * " + optimizedWeight + ";\n";
            shaderString += "    sum += texture2D(inputImageTexture, blurCoordinates[" + ((currentBlurCoordinateIndex * 2) + 2) + "]).rgb * " + optimizedWeight + ";\n";
        }

        // If the number of required samples exceeds the amount we can pass in via varyings, we have to do dependent texture reads in the fragment shader
        if (trueNumberOfOptimizedOffsets > numberOfOptimizedOffsets)
        {
            shaderString += "   vec2 singleStepOffset = vec2(texelWidthOffset, texelHeightOffset);\n";
            for (int currentOverlowTextureRead = numberOfOptimizedOffsets; currentOverlowTextureRead < trueNumberOfOptimizedOffsets; currentOverlowTextureRead++)
            {
                float firstWeight = standardGaussianWeights[currentOverlowTextureRead * 2 + 1];
                float secondWeight = standardGaussianWeights[currentOverlowTextureRead * 2 + 2];

                float optimizedWeight = firstWeight + secondWeight;
                float optimizedOffset = (firstWeight * (currentOverlowTextureRead * 2 + 1) + secondWeight * (currentOverlowTextureRead * 2 + 2)) / optimizedWeight;

                shaderString += "    sum += texture2D(inputImageTexture, blurCoordinates[0] + singleStepOffset * " + optimizedOffset + ").rgb * " + optimizedWeight + ";\n";
                shaderString += "    sum += texture2D(inputImageTexture, blurCoordinates[0] - singleStepOffset * " + optimizedOffset + ").rgb * " + optimizedWeight + ";\n";
            }
        }

        // Footer
        shaderString += "    gl_FragColor = vec4(sum,fragColor.a);\n"
                + "}\n";

        return shaderString;
    }

    public GPUImageGaussianBlurFilter() {
        this(2.0f);
    }

    public GPUImageGaussianBlurFilter(float blurSize) {
        this(blurSize, vertexShaderForBlur(blurSize), fragmentShaderForBlur(blurSize));
    }

    public GPUImageGaussianBlurFilter(float blurSize, String vertexShader, String fragmentShader) {
        super(vertexShader, fragmentShader, vertexShader, fragmentShader);
        this.mBlurSize = 0.0f;
        this.mBlurSize = blurSize;
    }

    public void setBlurSize(final float blurSize) {
        this.mBlurSize = blurSize;
        runOnDraw(new Runnable() {
            public void run() {
                for (GPUImageFilter filter : GPUImageGaussianBlurFilter.this.mFilters) {
                    filter.destroy();
                }
                String vertexShader = GPUImageGaussianBlurFilter.vertexShaderForBlur(blurSize);
                String fragmentShader = GPUImageGaussianBlurFilter.fragmentShaderForBlur(blurSize);
                GPUImageGaussianBlurFilter.this.mFilters.clear();
                GPUImageGaussianBlurFilter.this.addFilter(new GPUImageFilter(vertexShader, fragmentShader));
                GPUImageGaussianBlurFilter.this.addFilter(new GPUImageFilter(vertexShader, fragmentShader));
                GPUImageGaussianBlurFilter.this.updateMergedFilters();
                for (GPUImageFilter filter2 : GPUImageGaussianBlurFilter.this.mFilters) {
                    if (GPUImageGaussianBlurFilter.this.isInitialized()) {
                        filter2.init();
                    }
                    if (!(GPUImageGaussianBlurFilter.this.mOutputWidth == 0 || GPUImageGaussianBlurFilter.this.mOutputHeight == 0)) {
                        filter2.onOutputSizeChanged(GPUImageGaussianBlurFilter.this.mOutputWidth, GPUImageGaussianBlurFilter.this.mOutputHeight);
                    }
                }
                GPUImageGaussianBlurFilter.this.initTexelOffsets();
            }
        });
    }

//    @Override
//    public float getHorizontalTexelOffsetRatio() {
//        return mBlurSize;
//    }
//
//    @Override
//    public float getVerticalTexelOffsetRatio() {
//        return mBlurSize;
//    }
}