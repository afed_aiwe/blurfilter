package com.gpu_blur.filters;
import android.util.SizeF;

public class GPUImageGaussianRoundRectFilter extends GPUImageTwoInputFilter {
    public static final String FRAGMENT_SHADER =
    "uniform sampler2D inputImageTexture;\n" +
            "uniform sampler2D inputImageTexture2;\n" +
            "varying vec2 textureCoordinate;\n" +
            "varying vec2 textureCoordinate2;\n" +
            "uniform vec2 rectSize;\n" +
            "uniform float cornerRadius;\n" +
            "uniform vec2 iResolution;\n"+
            "float distRect(vec2 pixel, vec2 rect_center, vec2 rect_size) {\n" +
            "   vec2 d = abs(rect_center - pixel) - rect_size * 0.5;\n"+
            "   return length(max(d, 0.0));\n" +
            "}\n" +

            "float distRoundRect(vec2 pixel, vec2 rect_center, vec2 rect_size, float rect_round) {\n" +
            "   return distRect(pixel, rect_center, rect_size - vec2(rect_round * 2.0)) - rect_round;\n"+
            "}\n" +

            "void main(){\n" +
            "   vec4 blurredImageColor = texture2D(inputImageTexture, textureCoordinate);\n" +
            "   vec4 sharpImageColor = texture2D(inputImageTexture2, textureCoordinate);\n" +
            "   float d = distRoundRect(gl_FragCoord.xy, vec2(iResolution.x / 2.0, iResolution.y / 2.0), vec2(rectSize.x, rectSize.y), cornerRadius);\n" +
            "   gl_FragColor = mix(blurredImageColor, sharpImageColor, step(0.0, d));\n" +
            "}\n";
    private SizeF rectSize;
    private float cornerRadius;
    private SizeF resolution;

    private int rectSizeLocation = getUniformLocation( "rectSize");
    private int cornerRadiusLocation = getUniformLocation("cornerRadius");
    private int resolutionLocation = getUniformLocation("iResolution");

    protected GPUImageGaussianBlurFilter mGaussianBlurFilter;

    public GPUImageGaussianRoundRectFilter(float blurSize, SizeF rectSize, float cornerRadius, SizeF resolution) {
        super(FRAGMENT_SHADER);
        this.rectSize = rectSize;
        this.cornerRadius = cornerRadius;
        this.resolution = resolution;
        mGaussianBlurFilter = new GPUImageGaussianBlurFilter(blurSize);
        setSecondFilter(mGaussianBlurFilter);
    }

    public void onInit() {
        super.onInit();
        setRectSize(this.rectSize);
        setCornerRadius(this.cornerRadius);
        setResolution(this.resolution);
    }

    public void setResolution(SizeF resolution) {
        this.resolution = resolution;
        setTwoFloat(this.resolutionLocation, this.resolution.getWidth(), this.resolution.getHeight());
    }

    public void setCornerRadius(float cornerRadius) {
        this.cornerRadius = cornerRadius;
        setFloat(cornerRadiusLocation, this.cornerRadius);
    }

    public void setRectSize(SizeF rectSize) {
        this.rectSize = rectSize;
        setTwoFloat(rectSizeLocation, this.rectSize.getWidth(), this.rectSize.getHeight());
    }

    public void setBlurSize(float blurSize) {
        this.mGaussianBlurFilter.setBlurSize(blurSize);
    }
}
