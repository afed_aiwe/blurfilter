package jp.co.cyberagent.android.gpuimage.filter;

import java.util.List;

class GPUImageCompositeFilter extends GPUImageTwoInputFilter {

    private List<GPUImageFilter> filters;

    public GPUImageCompositeFilter(String fragmentShader) {
        super(fragmentShader);
    }
}
