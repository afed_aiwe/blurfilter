package jp.co.cyberagent.android.gpuimage.filter;

import android.opengl.GLES20;
import android.util.SizeF;

public class GPUImageGaussianBlurPositionFilter extends GPUImageTwoPassTextureSamplingFilter {

    public static final String VERTEX_SHADER =
            "attribute vec4 position;\n" +
                    "attribute vec4 inputTextureCoordinate;\n" +
                    "\n" +
                    "const int GAUSSIAN_SAMPLES = 9;\n" +
                    "\n" +
                    "uniform float texelWidthOffset;\n" +
                    "uniform float texelHeightOffset;\n" +
                    "\n" +
                    "varying vec2 textureCoordinate;\n" +
                    "varying vec2 blurCoordinates[GAUSSIAN_SAMPLES];\n" +
                    "\n" +
                    "void main()\n" +
                    "{\n" +
                    "	gl_Position = position;\n" +
                    "	textureCoordinate = inputTextureCoordinate.xy;\n" +
                    "	\n" +
                    "	// Calculate the positions for the blur\n" +
                    "	int multiplier = 0;\n" +
                    "	vec2 blurStep;\n" +
                    "   vec2 singleStepOffset = vec2(texelHeightOffset, texelWidthOffset);\n" +
                    "    \n" +
                    "	for (int i = 0; i < GAUSSIAN_SAMPLES; i++)\n" +
                    "   {\n" +
                    "		multiplier = (i - 4);\n" +
                    "       // Blur in x (horizontal)\n" +
                    "       blurStep = float(multiplier) * singleStepOffset;\n" +
                    "		blurCoordinates[i] = inputTextureCoordinate.xy + blurStep;\n" +
                    "	}\n" +
                    "}\n";

    public static final String FRAGMENT_SHADER =
            "uniform sampler2D inputImageTexture;\n" +
                    "\n" +
                    "const lowp int GAUSSIAN_SAMPLES = 9;\n" +
                    "\n" +
                    "varying highp vec2 textureCoordinate;\n" +
                    "varying highp vec2 blurCoordinates[GAUSSIAN_SAMPLES];\n" +
                    "\n" +
                    "uniform vec2 rectSize;\n" +
                    "uniform float cornerRadius;\n" +
                    "uniform vec2 iResolution;\n"+
                    "\n"+
                    "float distRect(vec2 pixel, vec2 rect_center, vec2 rect_size) {\n" +
                    "   vec2 d = abs(rect_center - pixel) - rect_size * 0.5;\n"+
                    "   return length(max(d, 0.0));\n" +
                    "}\n" +

                    "float distRoundRect(vec2 pixel, vec2 rect_center, vec2 rect_size, float rect_round) {\n" +
                    "   return distRect(pixel, rect_center, rect_size - vec2(rect_round * 2.0)) - rect_round;\n"+
                    "}\n" +
                    "void main()\n" +
                    "{\n" +
                    "   float d = distRoundRect(gl_FragCoord.xy, vec2(iResolution.x / 2.0, iResolution.y / 2.0), vec2(rectSize.x, rectSize.y), cornerRadius);\n" +
                    "   float c = step(0.0, d);\n" +
                    "   if(c == 1.0)\n"+
                    "   {\n"+
                    "	    lowp vec3 sum = vec3(0.0);\n" +
                    "       lowp vec4 fragColor = texture2D(inputImageTexture,textureCoordinate);\n" +
                    "	    \n" +
                    "       sum += texture2D(inputImageTexture, blurCoordinates[0]).rgb * 0.05;\n" +
                    "       sum += texture2D(inputImageTexture, blurCoordinates[1]).rgb * 0.09;\n" +
                    "       sum += texture2D(inputImageTexture, blurCoordinates[2]).rgb * 0.12;\n" +
                    "       sum += texture2D(inputImageTexture, blurCoordinates[3]).rgb * 0.15;\n" +
                    "       sum += texture2D(inputImageTexture, blurCoordinates[4]).rgb * 0.18;\n" +
                    "       sum += texture2D(inputImageTexture, blurCoordinates[5]).rgb * 0.15;\n" +
                    "       sum += texture2D(inputImageTexture, blurCoordinates[6]).rgb * 0.12;\n" +
                    "       sum += texture2D(inputImageTexture, blurCoordinates[7]).rgb * 0.09;\n" +
                    "       sum += texture2D(inputImageTexture, blurCoordinates[8]).rgb * 0.05;\n" +
                    "       \n" +
                    "	    gl_FragColor = vec4(sum,fragColor.a);\n" +
                    "   }\n"+
                    "   else\n" +
                    "   {\n"+
                    "       gl_FragColor = texture2D(inputImageTexture, textureCoordinate);\n" +
                    "   }\n" +
                    "}";

    public GPUImageGaussianBlurPositionFilter() {
        this(1f);
    }

    public GPUImageGaussianBlurPositionFilter(float blurSize) {
        super(VERTEX_SHADER, FRAGMENT_SHADER, VERTEX_SHADER, FRAGMENT_SHADER);
        this.blurSize = blurSize;
    }

    public GPUImageGaussianBlurPositionFilter(float blurSize, SizeF rectSize, float cornerRadius, SizeF resolution) {
        super(VERTEX_SHADER, FRAGMENT_SHADER, VERTEX_SHADER, FRAGMENT_SHADER);
        this.blurSize = blurSize;
        this.rectSize = rectSize;
        this.cornerRadius = cornerRadius;
        this.resolution = resolution;
        hasSelective = true;
    }

    private float blurSize;
    private boolean hasSelective = false;
    private SizeF rectSize;
    private float cornerRadius;
    private SizeF resolution;

    private int rectSizeLocation;
    private int cornerRadiusLocation;
    private int resolutionLocation;

    @Override
    public void onInit() {
        super.onInit();
        rectSizeLocation = GLES20.glGetUniformLocation(getProgram(), "rectSize");
        cornerRadiusLocation = GLES20.glGetUniformLocation(getProgram(), "cornerRadius");
        resolutionLocation = GLES20.glGetUniformLocation(getProgram(), "iResolution");
    }

    @Override
    public void onInitialized() {
        super.onInitialized();
        if (hasSelective) {
            setSelectiveParams(blurSize, rectSize, cornerRadius, resolution);
        } else {
            setBlurSize(blurSize);
        }
    }

    @Override
    public float getVerticalTexelOffsetRatio() {
        return blurSize;
    }

    @Override
    public float getHorizontalTexelOffsetRatio() {
        return blurSize;
    }

    /**
     * A multiplier for the blur size, ranging from 0.0 on up, with a default of 1.0
     *
     * @param blurSize from 0.0 on up, default 1.0
     */
    public void setBlurSize(float blurSize) {
        this.blurSize = blurSize;
        runOnDraw(new Runnable() {
            @Override
            public void run() {
                initTexelOffsets();
            }
        });
    }

    public void setSelectiveParams(float blurSize, SizeF rectSize, float cornerRadius, SizeF resolution) {
        this.blurSize = blurSize;
        this.rectSize = rectSize;
        this.cornerRadius = cornerRadius;
        this.resolution = resolution;
        setTwoFloat(rectSizeLocation, this.rectSize.getWidth(), this.rectSize.getHeight());
        setFloat(cornerRadiusLocation, this.cornerRadius);
        setTwoFloat(resolutionLocation, this.resolution.getWidth(), this.resolution.getHeight());
        runOnDraw(new Runnable() {
            @Override
            public void run() {
                initTexelOffsets();
            }
        });
    }

}
