package jp.co.cyberagent.android.gpuimage.filter;

import android.graphics.Point;
import android.util.SizeF;

import java.lang.reflect.Array;
import java.util.Arrays;

public class GPUImageGaussianSelectiveBlurFilter extends GPUImageFilterGroup {

    private GPUImageSelectiveFilter gpuImageSelectiveFilter;
//    private GPUImageOldGaussianBlurFilter blurFilter;
    private GPUImageGaussianBlurFilter blurFilter;

    protected float blurSize;
    private SizeF rectSize;
    private float cornerRadius;
    private SizeF resolution;


    public GPUImageGaussianSelectiveBlurFilter(float blurSize, SizeF rectSize, float cornerRadius, SizeF resolution) {
        this.blurSize = blurSize;
        this.rectSize = rectSize;
        this.cornerRadius = cornerRadius;
        this.resolution = resolution;
//        blurFilter = new GPUImageOldGaussianBlurFilter(this.blurSize, true);
        blurFilter = new GPUImageGaussianBlurFilter(this.blurSize);
        addFilter(blurFilter);
        gpuImageSelectiveFilter = new GPUImageSelectiveFilter(this.rectSize, this.cornerRadius, this.resolution);
        addFilter(gpuImageSelectiveFilter);
//        getFilters().add(blurFilter);
    }

    @Override
    public void onInitialized() {
        super.onInitialized();
        setBlurSize(blurSize);
        setSelectiveParams(rectSize, cornerRadius, resolution);
    }

    public void setBlurSize(float blurSize) {
//        blurFilter.setBlurRadiusInPixels(blurSize);
        blurFilter.setBlurSize(blurSize);
    }

    public void setSelectiveParams(SizeF rectSize, float cornerRadius, SizeF resolution) {
        gpuImageSelectiveFilter.setSelectiveParams(rectSize, cornerRadius, resolution);
    }
}
