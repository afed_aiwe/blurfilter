package jp.co.cyberagent.android.gpuimage.filter;

import java.util.Arrays;

class GPUImageMultiPassFilter extends GPUImageFilterGroup {

    public GPUImageMultiPassFilter(String firstVertexShader, String firstFragmentShader,
                                   String secondVertexShader, String secondFragmentShader) {
        super(null);
        if (firstVertexShader != null && firstFragmentShader != null) {
            addFilter(new GPUImageFilter(firstVertexShader, firstFragmentShader));
        }
        if (secondVertexShader != null && secondFragmentShader != null) {
            addFilter(new GPUImageFilter(secondVertexShader, secondFragmentShader));
        }
    }
}
