package jp.co.cyberagent.android.gpuimage.filter;

import android.opengl.GLES20;
import android.util.SizeF;

public class GPUImageSelectiveFilter extends GPUImageTwoInputFilter {
    public static final String SELECTIVE_FRAGMENT_SHADER =
            //this is overlay
            "uniform sampler2D inputImageTexture;\n" +
                    "uniform sampler2D inputImageTexture2;\n" +
                    "varying vec2 textureCoordinate;\n" +
                    "varying vec2 textureCoordinate2;\n" +
                    "uniform vec2 rectSize;\n" +
                    "uniform float cornerRadius;\n" +
                    "uniform vec2 iResolution;\n"+
                    "float distRect(vec2 pixel, vec2 rect_center, vec2 rect_size) {\n" +
                    "   vec2 d = abs(rect_center - pixel) - rect_size * 0.5;\n"+
                    "   return length(max(d, 0.0));\n" +
                    "}\n" +

                    "float distRoundRect(vec2 pixel, vec2 rect_center, vec2 rect_size, float rect_round) {\n" +
                    "   return distRect(pixel, rect_center, rect_size - vec2(rect_round * 2.0)) - rect_round;\n"+
                    "}\n" +

                    "void main(){\n" +
                    "   vec4 blurredImageColor = texture2D(inputImageTexture, textureCoordinate);\n" +
                    "   vec4 sharpImageColor = texture2D(inputImageTexture2, textureCoordinate2);\n" +
                    "   float d = distRoundRect(gl_FragCoord.xy, vec2(iResolution.x / 2.0, iResolution.y / 2.0), vec2(rectSize.x, rectSize.y), cornerRadius);\n" +
//                    "   gl_FragColor = mix(sharpImageColor, selectiveImageColor, clamp(d - 0.5, 0.0, 1.0));\n" +
                    "   gl_FragColor = mix(sharpImageColor, blurredImageColor, step(0.0, d));\n" +
                    "}\n";

    private SizeF rectSize;
    private float cornerRadius;
    private SizeF resolution;

    private int rectSizeLocation;
    private int cornerRadiusLocation;
    private int resolutionLocation;

    public GPUImageSelectiveFilter() {
        super(SELECTIVE_FRAGMENT_SHADER);
    }

    public GPUImageSelectiveFilter(SizeF rectSize, float cornerRadius, SizeF resolution) {
        super(SELECTIVE_FRAGMENT_SHADER);
        this.rectSize = rectSize;
        this.cornerRadius = cornerRadius;
        this.resolution = resolution;
    }

    @Override
    public void onInit() {
        super.onInit();
        rectSizeLocation = GLES20.glGetUniformLocation(getProgram(), "rectSize");
        cornerRadiusLocation = GLES20.glGetUniformLocation(getProgram(), "cornerRadius");
        resolutionLocation = GLES20.glGetUniformLocation(getProgram(), "iResolution");
    }

    @Override
    public void onInitialized() {
        super.onInitialized();
        setSelectiveParams(rectSize, cornerRadius, resolution);
    }

    public void setSelectiveParams(final SizeF rectSize, final float cornerRadius, final SizeF resolution) {
        this.rectSize = rectSize;
        this.cornerRadius = cornerRadius;
        this.resolution = resolution;
        setTwoFloat(rectSizeLocation, this.rectSize.getWidth(), this.rectSize.getHeight());
        setFloat(cornerRadiusLocation, this.cornerRadius);
        setTwoFloat(resolutionLocation, this.resolution.getWidth(), this.resolution.getHeight());

    }
}
